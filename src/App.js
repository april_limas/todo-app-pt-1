import React, { useState } from "react";
import todosList from "./todos.json";

const App = () => {
  const currentId = Math.floor(Math.random() * 100)
  const [todos, setTodos] = useState(todosList)
  const [title, setTitle] = useState('')
  

  const addToDo = (e) => {
    if (e.key === "Enter") {
      setTodos(
        [...todos, {
          "userId": 1,
          "id": currentId,
          "title": e.target.value,
          "completed": false
        }]
      )
      e.target.value = ""
    }
  }
 
    const handleCheck = id => e => {
    let toggleTodos = todos.map (todo => {
      if (todo.id === id) {
       return {...todo, completed: !todo.completed}
      }
        return {...todo}
    })
      setTodos(toggleTodos)
    }

    const handleDelete = id => e => {
      const newTodos = todos.filter (todo => todo.id !== id )
      setTodos(newTodos)
    }

    const clearCompleted = () => {
      const newTodos = todos.filter (todo => todo.completed === false)
      setTodos(newTodos)
    }
      
  
  
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
            <input className="new-todo" placeholder="What needs to be done?" autoFocus onKeyDown={addToDo}
            />
        </header>
        <TodoList todos={todos} handleCheck={handleCheck} handleDelete={handleDelete}/>
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={clearCompleted}>Clear completed</button>
        </footer>
      </section>
    );
}
          
const TodoItem = ({title, completed, handleCheck, handleDelete}) => {
  
  return (
    <li className={completed ? "completed" : ""}>
      <div className="view">
        <input className="toggle" 
               type="checkbox"
               checked={completed}
               onChange={handleCheck}
        />
        <label>{title}</label>
        <button className="destroy" onClick={handleDelete} />
      </div>
    </li>
  );
}        



const TodoList = ({todos, handleCheck, handleDelete}) => {
  
    return (
      <section className="main">
        <ul className="todo-list">
          {todos.map((todo) => (
            <TodoItem key={todo.id} title={todo.title} completed={todo.completed} handleCheck={handleCheck(todo.id)} handleDelete={handleDelete(todo.id)} />
            ))}
        </ul>
      </section>
    );
}





export default App;
